import { ReactElement } from 'react'
import { GetStaticProps } from 'next'
import { Header } from 'hugoworld-components'
import Skateboard from '../components/atoms/Skateboard'
import { fetchAPI } from '../utils/strapi'
import { AboutViewData, StrapiLocales } from '../types/global'

import styles from './about.module.scss'

interface SectionProps {
  header: string
  content: string
  scene: (() => JSX.Element | null) | null
}

const SectionRight = ({ header, content, scene: Scene }: SectionProps): JSX.Element => (
  <div className={`row ${styles.section}`}>
    <div className="col-md-6 col-sm-12 col-lg-6">{Scene ? <Scene /> : null}</div>
    <div className={`col-md-6 col-sm-12 col-lg-6 ${styles.content}`}>
      <div>
        <h4 className={styles.header}>{header}</h4>
        {content}
      </div>
    </div>
  </div>
)

const SectionLeft = ({ header, content, scene: Scene }: SectionProps): JSX.Element => (
  <div className={`row ${styles.section}`}>
    <div className={`col-md-6 col-sm-12 col-lg-6 ${styles.content}`}>
      <div>
        <h4 className={styles.header}>{header}</h4>
        {content}
      </div>
    </div>
    <div className="col-md-6 col-sm-12 col-lg-6">{Scene ? <Scene /> : null}</div>
  </div>
)

const About = ({ data, locale }: Props): ReactElement => {
  console.log(data)

  return (
    <div className={`container ${styles.viewBox}`}>
      <div className="row mt-4 text-center">
        <div className="col">
          <span className="d-inline-block">
            <Header as="h1">About Me</Header>
          </span>
        </div>
      </div>
      <SectionRight
        header="Lorem Ipsum"
        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas egestas maximus.
            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
            egestas. Morbi imperdiet elit non efficitur congue. Phasellus eleifend finibus neque sit
            amet eleifend. Maecenas condimentum, purus sed finibus malesuada, risus libero tempus
            dolor, sit amet elementum nulla elit et enim. Nullam aliquet, diam sit amet rhoncus
            sollicitudin, arcu lacus vulputate ex, ac congue nibh leo at ipsum. Ut facilisis laoreet
            eros, eu auctor turpis."
        scene={Skateboard}
      />
      <SectionLeft
        header="Lorem Ipsum"
        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas egestas maximus.
            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
            egestas. Morbi imperdiet elit non efficitur congue. Phasellus eleifend finibus neque sit
            amet eleifend. Maecenas condimentum, purus sed finibus malesuada, risus libero tempus
            dolor, sit amet elementum nulla elit et enim. Nullam aliquet, diam sit amet rhoncus
            sollicitudin, arcu lacus vulputate ex, ac congue nibh leo at ipsum. Ut facilisis laoreet
            eros, eu auctor turpis."
        scene={null}
      />
      <SectionRight
        header="Lorem Ipsum"
        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas egestas maximus.
            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
            egestas. Morbi imperdiet elit non efficitur congue. Phasellus eleifend finibus neque sit
            amet eleifend. Maecenas condimentum, purus sed finibus malesuada, risus libero tempus
            dolor, sit amet elementum nulla elit et enim. Nullam aliquet, diam sit amet rhoncus
            sollicitudin, arcu lacus vulputate ex, ac congue nibh leo at ipsum. Ut facilisis laoreet
            eros, eu auctor turpis."
        scene={null}
      />
      {/* About Hugo in <i className={`flag flag-${getStrapiLocale(locale)}`} /> language */}
    </div>
  )
}

interface Props {
  data: AboutViewData
  locale: StrapiLocales
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { locale } = context
  const data: AboutViewData = await fetchAPI('/about-view')

  return {
    props: {
      data,
      locale,
    },
  }
}

export default About
