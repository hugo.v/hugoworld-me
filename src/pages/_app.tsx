import type { AppProps } from 'next/app'
import Head from 'next/head'
import { ReactElement } from 'react'

import WithNavigation from '../layouts/navigation/WithNavigation'

import '../styles/app.scss'

const App = ({ Component, pageProps }: AppProps): ReactElement => {
  return (
    <>
      <Head>
        <title>HugoWorld</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <WithNavigation>
        <Component {...pageProps} />
      </WithNavigation>
    </>
  )
}

export default App
