import { useState, useEffect } from 'react'

const useScrollTop = (): boolean => {
  const [scrollTop, setScrollTop] = useState(true)

  useEffect(() => {
    window.onscroll = () => {
      if (window.pageYOffset === 0) {
        setScrollTop(true)
      } else if (scrollTop) setScrollTop(false)
    }
    if (window.pageYOffset === 0 && !scrollTop) setScrollTop(true)
    return () => {
      window.onscroll = null
    }
  }, [scrollTop])

  return scrollTop
}

export default useScrollTop
