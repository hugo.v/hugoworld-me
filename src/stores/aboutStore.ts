import create from 'zustand'

export const useAboutStore = create((set) => ({
  hello: 'World',
  setHello: (hello: string) => set(() => ({ hello })),
}))
