import { ReactElement, ReactNode } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

// Hooks
import useScrollTop from '../../hooks/useScrollTop'

import styles from './WithNavigation.module.scss'

interface Props {
  children: ReactNode
}

interface RouteInterface {
  path: string
  label: {
    [key: string]: string
  }
}

const routes: RouteInterface[] = [
  {
    path: '/blog',
    label: {
      fr: 'Blog',
      'en-US': 'Blog',
    },
  },
  {
    path: '/about',
    label: {
      fr: 'A Propos',
      'en-US': 'About',
    },
  },
]

const WithNavigation = ({ children }: Props): ReactElement => {
  const { locale, pathname } = useRouter()
  const isScrollTop = useScrollTop()

  return (
    <div>
      <div className={`${styles.nav} ${isScrollTop ? styles.navTop : styles.navSticky}`}>
        <div className={`container ${styles.navContent}`}>
          <div className={styles.left}>
            <div className={styles.brand}>
              <Link href="/">
                <a>HugoWorld</a>
              </Link>
            </div>
          </div>
          <div className={styles.right}>
            <ul className={styles.links}>
              {routes.map((route) => (
                <li className={styles.link} key={`route_${route.path}`}>
                  <Link href={route.path}>
                    <a>{route.label[locale || 'en-US']}</a>
                  </Link>
                </li>
              ))}
            </ul>
            <div className={styles.flags}>
              <Link href={pathname} locale="en-US">
                <i className="flag flag-en me-2" />
              </Link>
              <Link href={pathname} locale="fr">
                <i className="flag flag-fr" />
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className={`${styles.children}`}>{children}</div>
    </div>
  )
}

export default WithNavigation
