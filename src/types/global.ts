export interface AboutViewData {
  id: number
  published_at: string
  created_at: string
  updated_at: string
  title: {
    fr: string
    en: string
  }
  bio: {
    fr: string
    en: string
  }
  thumbnail: {
    name: string
    alternativeText: string
    caption: string
    width: number
    height: number
    formats: {}
    hash: string
    ext: string
    mime: string
    size: number
    url: number
    previewUrl: null | string
    provider: string
    provider_metadata: {}
    created_at: string
    updated_at: string
  }
}

export type StrapiLocales = 'en-US' | 'fr'
